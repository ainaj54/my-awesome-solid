package no.noroff.nicholas.scenario2;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;
    private boolean alarmOn = false;

    public void check(PsiPressurePopable sensor){
        double psiPressureValue = sensor.popNextPressurePsiValue(); //Overengineering?
        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
