package no.noroff.nicholas.scenario2;

public interface PsiPressurePopable {
    double popNextPressurePsiValue();
}
