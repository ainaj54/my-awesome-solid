package no.noroff.nicholas.scenario2;

import java.util.Random;

public class Sensor implements PsiPressurePopable {
    public static final double OFFSET = 16;             //necessary? Include in sample pressure function

    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;                  //why here? why not?
        pressureTelemetryValue = samplePressure();      //Can take the samplePressure directly. Why have temp value in between?
        return OFFSET + pressureTelemetryValue;         //include offset in sample pressure
    }

    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();//
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();// separate class to generate pressurevalues
        return pressureTelemetryValue;
    }
}
