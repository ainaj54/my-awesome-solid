package no.noroff.nicholas.scenario1;

public class Alarm {
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    Sensor sensor = new Sensor(); //Encpsulation Also, Dependency Inversion
    boolean alarmOn = false; //Encapsulation. should be private.

    public void check(){    // have sensorobject as input maybe not even necessary to have sensor as field.
        double psiPressureValue = sensor.popNextPressurePsiValue(); // potential open/close issues if we want to check other sensors. possible overengineering from my part.

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        return alarmOn;
    }
}
