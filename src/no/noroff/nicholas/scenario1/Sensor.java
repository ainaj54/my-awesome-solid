package no.noroff.nicholas.scenario1;

import no.noroff.nicholas.scenario2.PsiPressurePopable;

import java.util.Random;

public class Sensor implements PsiPressurePopable {
    public static final double OFFSET = 16;             //necessary? Include in sample pressure function

    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;                  //why here? why not?
        pressureTelemetryValue = samplePressure();      //Can take the samplePressure directly. Why have temp value in between? Might be good to understans that
        return OFFSET + pressureTelemetryValue;         //include offset in sample pressure
    }

    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random(); //Is it dependency inversion if it's an internal Java dependency?
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();// Can argue single responsibility violation.
        return pressureTelemetryValue;
    }
}
